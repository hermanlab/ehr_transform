======================================
EHR_Transform
======================================

EHR_Transform
    Query UPHS Penn Data Store and Clarity to retrieve clinical data for a defined cohort. Deidentify, process, and load into sqlite db <htn_all_pk_enc_DEID.sqlite>

.. contents:: Table of Contents

why?
====

* Retrieve de-identified clinical data for analysis

installation
============

setup environment
-----------------
Clone repository::

  git clone git@bic.med.upenn.edu:hermanlab/EHR_transform.git myproject
  cd myproject

Create `oracle folder::

  mkdir oracle
  # Download instantclient_11_2.zip from Oracle website (see below).


Download instantclient_11_2.zip from Oracle website and put it in `oracle` folder:

- Oracle Client version: 11.2.0.4.0, Instant Client Pakcage - Basic

- Oracle download: `Oracle instant client <https://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html>`_

- Change name of the downloaded zip file to ``instantclient_11_2.zip``


Setup docker container::

  nano Dockerfile  # create useradd for <user>

  docker build -f Dockerfile --tag ehr_transform2 .
  docker run -d --rm -p 40030:8787 --storage-opt size=30G --name EHR_TRANSFORM -v `pwd`:/home/<user> -v /data/projects/data:/data:ro ehr_transform2

  docker exec -it -u <user> EHR_TRANSFORM /bin/bash
  userdel hermanda   # May need to exec without user if sudo not working
  userdel dingxi
  passwd <user>
  exit

NOTE: ``/data/projects/data`` folder (a directory outside of container) is essential for storing research DB data.

gpg
---
In current pipeline, gpg (version 1.4.22) was installed. In order to use it, a private/public key should be set, OR imported from outside.

1. Set in container
This will generate a new gpg private/public key set for current user, and it allows for encryption/decryption based on that user. (NOTE: the old gpg file that was created beforehand could not be decrypted by this key set, since the old gpg does not have information of this newer key set.)

To set new key::

  % gpg --gen-key

and follow instruction to set Name, email, and passphrase (and some other stuff..)

2. Export/Import
This will allow the user to use their previously owned gpg keys.

To export public/private key from another location (outside of docker container)::

  % gpg --export <your@email.com> > <file>
  % gpg --export-secret-keys <your@email.com> > <file>
  # copy private and public key pair to container

To import public/private keys (inside docker container)::

  % gpg --import <keyfile>

(This version of gpg allows user to import private key. For more detail of gpg, see: https://www.gnupg.org)

configuration
-------------
``config.ini`` could be created by modifying the template ``config_template.ini``.

1. In the pipeline section latest version, change the seed to random integer.

2. If it's a new project, add section::

    <project name>]
    left_censor_date_chr = YYYY-MM-DD
    prj_dir = <project>
    chunk_size = 1000

architecture
============

This project has the following subdirectories or files:

* ``config_template.ini`` - template for config file
* ``data`` - temporary data folder or intermediate files
* ``dev`` - development tools not essential for the primary functionality of the application. [To create]
* ``deidentify_pt_date_mapping.R`` - executable R script for getting date shifts for patients whose age are greater than 18
* ``doc`` - files related to project documentation. [To create]
* ``Dockerfile`` - Docker container build-up file
* ``EHR_transform`` - pipeline files: subcommands, and reference files
* ``ehr_transform.py`` - main entrance script for python subcommands
* ``logs`` - log directory, default folder when specified `--log2dir`
* ``R`` - supplementary R functions
* ``raw_2_PK_ENC_level.R`` - executable R script that pushes queried files into database
* ``README.rst`` - Instructions of the pipeline
* ``scripts`` - some handy bash scripts
* ``SQL`` - SQL files to execute, either from PDS or Clarity
* ``testfiles`` - files and data used for testing.
* ``tests`` - subpackage implementing unit tests.



Note that ``ehr_transform.py`` and ``EHR_transform`` are placeholder names that
are replaced with your script and project names during setup.


help
=========
Commands are constructed as follows. Every command starts with the name of the script, followed by an "action" followed by a series of required or optional "arguments". The name of the script, the action, and options and their arguments are entered on the command line separated by spaces. Help text is available for both the ``ehr_transform`` script and individual actions using the ``-h`` or ``--help`` options.

In order to get help for usage of main function and actions, first go into project directory::

  % cd <EHR_transform>

If main function usage help is needed::

  % ./ehr_transform.py --help

If action usage help is needed::

  % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> help <subcommand>
  # for example:
  % ./ehr_transform.py --project htn_all --pplversion v0.2.211 -vv help query_pds

For help on gpg::

  % ./ehr_transform.py help gpg_files


execution
=========
Start from navigating to the project home folder::

  % cd <EHR_transform>
  % ./ehr_transform.py --help

all python subcommands could be directly run as::

  % ./ehr_transform.py --project <PA/sunil_pts> --pplversion <v0.2.3/or later> <subcommands>

If a profiler is needed, then just add "python3 -m cProfile -o profilename" into the front, as::

  % python3 -m cProfile -o profilename_you_want ./ehr_transform.py [--options] <subcommands>

then the profile which keeps records of running time for each individual step will be saved in file "profilename_you_want".

logging
=======
Currently, the logging system contains two options:

1. --logfile <FILENAME>: a file name shoule be provided in <FILENAME> part. NOTE that the dir must be existing.
2. --log2dir: FLAG.

For usage:

1. Log to Stderr: neither of two options should be specified
2. Log to FILE: only specify --logfile <FILENAME> option. Directory must exist
3. Log to ./logs folder: only specify --log2dir (which is a flag). By default, the directories will be created as ./logs/PROJECT/PipelineVersion/SUBCOMMANDS_log.txt

Pipeline execution
==================
To start the process, <PROJECT NAME> and <PIPELINE VERSION> should always be specified.


1. Define cohort:
In DataGrip, execute ``htn_all.sql`` (or equivalent) (for testing, use ``htn_all.test.sql`` instead) to get a list of EMPI's for the whole cohort, and store them in RAR_FULL_PTS [table] in the user's scheme in PDS, along with generated UNI_ID, which will serve as a deidentified patient ID in the project. And write this table out to csv file: ``EMPI_UNIID.csv`` and put it under raw_data/PROJECT/PIPELINE_VERSION/ folder. Also, another sql file should also be executed, ``htn_all_FIX_age_filter.sql``, which are used to collect all birthdates for age filter. A csv file, named ``HTN_ALL_AGE.csv``, should be created according to the resulting table. NOTE: These two csv files are MUST for the following steps.

2. Query PDS to describe cohort

To query PDS, in general::

  % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_pds ./SQL/extract_*** --db_usr <USER_NAME_PDS>


where ``extract_***`` are sql files in ``./SQL`` folder. Currently ENC, DX, LAB_EPIC, LAB_CERNER, DEMO, and EMPI_MRN tables could be queried this way. And the results will be in ``./data/raw_data/PROJECT/RAR_Version/***/`` folder, where ``***`` are what's been queried. Now, use --temp_table flag to control which method to use for query: (1) with ``--temp_table``, create TEMP EMPI Table and join to it; (2) without ``--temp_table``, use NESTED query to select patients.

NOTE: ``--db_usr`` is where to speicfy the PDS user, whose password will be required for connecting to PDS. Default is ``dingxi``. Please change in following queries.

    2.1 LAB_EPIC::

      % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_pds ./SQL/extract_lab_epic.sql --chunk_size 1000

    This could stop many times during the process, usually failing at the beginning of a new query. If this happens, first check whether the last csv file was successfully created (file exists, AND query finished); then relunch the query with flag ``--resume_run``::

      % python -m cProfile -o ./logs/htn_all/<PPLVERSION>/cprofile_lab_epic ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_pds ./SQL/extract_lab_epic.sql --resume_run


    2.2 LAB_CERNER::

      % python -m cProfile -o ./logs/htn_all/<PPLVERSION>/cprofile_lab_cerner ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_pds ./SQL/extract_lab_cerner.sql --temp_table

    2.3 ENC::

      % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_pds ./SQL/extract_enc.sql --chunk_size 1000 --temp_table

    2.4 Vitals::

      % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_pds ./SQL/extract_vitals.sql --chunk_size 1000 --temp_table

    2.5 DX::

      % python -m cProfile -o ./logs/htn_all/<PPLVERSION>/cprofile_dx ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --logfile ./logs/htn_all/v0.2.3.1/dx_log query_pds ./SQL/extract_dx.sql

    2.6 MED::

      % python -m cProfile -o ./logs/htn_all/<PPLVERSION>/cprofile_meds ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --logfile ./logs/htn_all/v0.2.3.1/meds_log query_pds ./SQL/extract_meds.sql

    2.7 DEMO::

      % python -m cProfile -o ./logs/htn_all/<PPLVERSION>/cprofile_demo ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_pds ./SQL/extract_demo.sql

    2.8 EMPI-MRN-UNI_ID::

      % python -m cProfile -o ./logs/htn_all/<PPLVERSION>/cprofile_uni_id_mrn ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --logfile ./logs/htn_all/v0.2.3.1/uni_id_mrn_log.txt query_pds ./SQL/extract_uni_id_mrn.sql --full_chunk

3. Query Clarity to describe cohort

To get notes from Clarity, in general::

  % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir query_notes --db_usr <DB_USER_NAME_Clarity>


By default, the file "EMPI_UNIID.csv" in data/raw_data/PROJECT/RAR_Version would be read in as full EMPI list. And several csv files (1000 EMPI each) would be created in data/raw_data/PROJECT/RAR_Version/notes_queried, named as note_1.csv, note_2.csv, ...

NOTE: ``--db_usr`` is an option here, with same function as in ``query_pds``.

4. Create Age Filter file
The sql file ``htn_all_FIX_age_filter.sql`` should be run in PDS, and the corresponding csv file should be created as ``ALL_BIRTHDATE.csv`` in raw data folder: ``data/raw_data/<PROJECT>/<VERSION>/``. A filter of age will be applied when creating de_date_mapping file based on this. Later, data will be filtered before pushed into deidentified DB. (NOTE: birthdates were also included in DEMO files, but it requires to read in all separate demo files to gather this information. This is the reason why keeping a single file for birthdates. Actually it could be replaced later)


5. Create de-identification mapping file for DATE's
A date shift from -14 to +14 days was randomly assigned to EACH patient (it could be controlled by SEED under data pipeline version section in config file), and it'll be used to deidentify all DATE's associated with that patient. Shifts are specified in whole days (i.e., -2, -1, 0, 1, 2, but no 1.2 days), and they are stored as SECONDs. A mapping file will be created::

  % ./deidentify_pt_date_mapping.R <PIPELINE VERSION> --project <PROJECT>


An mapping file will be generated and stored as ``data/processed_data/<PROJECT>/<RAR_PPLVERSION>/mapping_files/de_date_mapping.csv``

NOTE: in current version, an Age Filter is also applied in this mapping file: only selecting patients who are >= 18 yr old at the right censor date (2018-01-01). As a result, some warning may be generated when deidentifying patients, like "deidentify: Some patient ID does not exist in mapping file."



6. Run Script to put all data into deidentified sqlite
Each data type is processed separately. The output DB is ``htn_all_pk_enc_DEID.sqlite``.

  6.1 ENC::

    % ./raw_2_PK_ENC_level.R <PIPELINE_VERSION> --project <PROJECT>  --data_cat ENC

  6.2 Lab::

    % ./raw_2_PK_ENC_level.R <PIPELINE_VERSION> --project <PROJECT>  --data_cat LAB

  6.3 DX::

    % ./raw_2_PK_ENC_level.R <PIPELINE_VERSION> --project <PROJECT>  --data_cat DX

  6.4 Dx Mapping Table::

    % ./raw_2_PK_ENC_level.R <PIPELINE_VERSION> --project <PROJECT>  --data_cat ICD_DICT

  6.5 Vitals::

    % ./raw_2_PK_ENC_level.R <PIPELINE_VERSION> --project <PROJECT>  --data_cat VITALS

  6.6 DEMO::

    % ./raw_2_PK_ENC_level.R <PIPELINE_VERSION> --project <PROJECT>  --data_cat DEMO

  6.7 MED::

    % ./raw_2_PK_ENC_level.R <PIPELINE_VERSION> --project <PROJECT>  --data_cat MED

  6.8 Regex Counts

  Apply regular expressions to extracted note data::

  % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv --log2dir count_notes --workers <5> --pk_enc_id_level

By default, the folder ``data/raw_data/PROJECT/RAR_Version/notes_queried`` will be searched for all files as input, separately; count regex. Results will be pushed into result database as table ``notes``, with unique index on ``NOTE_ID``, and index on ``(UNI_ID, ENCOUNTER_DATE)``. Note: no intermediate table/csv files will be generated.

This process supports for multiprocessing, so ``--workers NUMBER`` are needed; if not, the default is 5; if ``--workers 1``, files will be processed sequentially. If the directory contains .gpg files, put the flag ``--gpg_indir`` and this will ONLY process .gpg files.

NOTE: if ``--gpg_indir`` is specified, a temporary file with same name but not .gpg extension will be created; this means, if there is already a corresponding .csv file, it WILL BE OVERWRITTEN AND THEN DELETED!

Post Processing
==================
There are some intermediate tables which are very useful to be constructed inside DB that could help following analysis. Note that all post processed tables will start with ``P_`` as table names in database.

1. Consolidate meds

``med`` table in database is raw, with each row indicating a record. The processed table will impute START dates and STOP dates first, and then consolidate meds in same ``Drug Group`` into one continuous record for a patient, or several if interrupted. NOTE: only anti HTN drugs are included here, due to the medication DICT (which means, a modified med DICT will expand med groups in consolidated table)::

    % ./R/scripts/consolidate_htn_meds.R <PIPELINE_VERSION> --project <PROJECT>

The resulting table is called ``P_med_htn_consolidated``.

NOTE:

1. no primary key was created for this table, but it is indexed on patient id, ``ORDER_START_DATE`` and ``ORDER_STOP_DATE``

2. ``Drug_Group`` and ``Class`` contained mixed groups, connected by "-", such as LOSARTAN-HCTZ. When counting, these should be broken up.


Cipher for ID's
========================
In processed DB (``htn_all_pk_enc_DEID.sqlite``), not all ID's are de-identified (they are same as those in PDS). But most of these are record IDs, rather than patient-identifiable ID's (except for physician ID). In order to de-identify these, we use python package ``Crypto.Cipher`` were used.


Also, for deidentification purpose, birthdates of patients whose age was >= 90 years old at 2018-01-01 will be all moved to 90 years ago from 2018-01-01, so that no elder patients could be identified by age. This was done by shifting ``BIRTH_DATE``.

Also, for demo, ``ZIP_distance_km was`` categorized into ``ZIP_CAT``.

Follow steps to fully deidentify DB:

(1) Create a cipher key file, named ``.cipher_key``. This contains 16-digit random characters, serving as the key for cipher. KEEP IT SAFE::

      % cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1 > .cipher_key
      % chmod 400 .cipher_key


(2) Cipher DB tables::

      % ./ehr_transform.py --project <PROJECT> --pplversion <PPLVERSION> -vv cipher_deid <TABLE_NAME>


``<TABLE_NAME>`` is the indicator of which table to be ciphered. Currently, it supports DB tables of: ``dx``, ``encounters``, ``lab``, ``med``, ``vitals``, ``demo``, ``icd_dict``, ``notes``, ``P_locs_censor_date``, ``P_med_htn_consolidated``, ``P_pts_primary_loc``, ``P_pts_resistant_htn``, ``P_rar_avs_test_result_code``.

NOTE: due to process of new phenotyping, some of tables may not be accepted (coded) in this script. It's an easy upgrade: add the table name into ``cipher_deid.py``, and specify which column to be ciphered in variable ``cols`` (if none, just name an empty ``cols``), or dropped in ``cols_drop``. Lastly, give the new table an index.




encryption/decryption
========================
Encryptin and Decryption could also be done using action ``gpg_files``.

For encryption::

  % ./ehr_transform.py gpg_files --encrypt --indir <some_dir> --recipients xiruo.ding@uphs.upenn.edu

Note that ALL FILES in ``SOME_DIR`` will be ENCRYPTED, including all sub directories! If logs are needed, specify ``-v`` before ``gpg_files`` action (NOTE only 1 verbosity is enough; if two, DEBUG info from gpg will also be printed out). If logs need to be written out to file, project and pipeline version should both be specified to make sure the log file is in the right location. By default, the output data will be in the same place as original file, and if encryption successful, the original one will be deleted; this could be turned of by using ``--not_del_origin`` flag. And there is no default recipient, so they should be specified in the format ``--recipients A --recipients B --recipients C ...``.

For decryption (much simpler...)::

  % ./ehr_transform.py gpg_files --decrypt --indir <some_dir>

Note that currently, the original gpg files will not be deleted. After execution, a passphrase corresponding to encryption step is needed. ALL FILES in ``some_dir`` will be decrypted, including all sub directories!



versions
========

The package version is defined using ``git describe --tags --dirty``
(see http://git-scm.com/docs/git-describe for details).  The version
information is updated and saved in the file ``ungapatchka/data/ver``
when ``setup.py`` is run (on installation, or even by executing
``python setup.py -h``). Run ``python setup.py check_version`` to make
sure that the stored version matches the output of ``git
describe --tags --dirty``.

Add a tag like this::

  git tag -a -m 'version 0.1.0' 0.1.0


unit tests
==========

Unit tests are implemented using the ``unittest`` module in the Python
standard library. The ``tests`` subdirectory is itself a Python
package that implements the tests. All unit tests can be run like this::

    % python setup.py test

A single unit test can be run by referring to a specific module,
class, or method within the ``tests`` package using dot notation::

    % python setup.py test --test-suite tests.test_utils

In current version, only one module to test regex was implemented.


license
=======

Copyright (c) 2019 Daniel Herman, Xiruo Ding

Released under the MIT License:

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
