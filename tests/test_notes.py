"""
Test subcommands.
"""

import logging
import pandas as pd
import sys

from EHR_transform.scripts.main import main
from .__init__ import TestBase

import unittest

from EHR_transform.note_fx import read_notes, notes_cleaning, add_regex_counts
from EHR_transform.data.regex_dict import regex_dict

log = logging.getLogger(__name__)


class TestNotes(unittest.TestCase):

    input_file = "testfiles/notes.csv"
    df = read_notes(dat_file = input_file, log=log)

    df = notes_cleaning(df, log=log)

    df = add_regex_counts(notes_df = df, regex_dict = regex_dict, log=log)


    def testNOTE_Clean(self,df=df):
        self.assertEqual(len(df), 6)
        self.assertNotIn('1005', df.EMPI.tolist())
        self.assertIn('1003', df.EMPI.tolist())
        self.assertEqual(df[df.NOTE_ID == '403'].iloc[0].NOTE_STATUS, 'Addendum')

    def testNOTE_Regex(self, df=df):

        self.assertEqual(df[df.NOTE_ID == '401'].iloc[0].re_htn, 3)
        self.assertEqual(df[df.NOTE_ID == '401'].iloc[0].re_htn_spec, 1)
        self.assertEqual(df[df.NOTE_ID == '402'].iloc[0].re_hyperaldo, 2)
        self.assertEqual(df[df.NOTE_ID == '403'].iloc[0].re_aldo_producing_adenoma, 2)
        self.assertEqual(df[df.NOTE_ID == '403'].iloc[0].re_aldo_producing_adenoma_spec, 1)
        self.assertEqual(df[df.NOTE_ID == '401'].iloc[0].re_word_count, 32)

    # self.assertRaises(SystemExit, main, ['notacommand'])
    @unittest.expectedFailure
    def testNOTE_Re_Hyperaldo(self, df=df):
        # this is not working correctly right now
        # recorded as issue #1
        self.assertEqual(df[df.NOTE_ID == '402'].iloc[0].re_hyperaldo_spec, 1)

    # def testExit02(self):
    #     self.assertRaises(SystemExit, main, ['-h'])

    # def test01(self):
    #     main(['template', 'infile', 'outfile', '--monkey-type', 'macaque'])
