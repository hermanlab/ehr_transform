#! /bin/bash
shopt -s extglob

# Decrypt all *.gpg files in $1
DIRECTORY=$1

if [ ! -d "$DIRECTORY" ]; then
    echo "ERROR: no such directory $DIRECTORY"
    exit
fi
echo "Processing directory: $DIRECTORY"

files=$DIRECTORY/!(*.gpg)
for f in $files; do
    echo "ENCRYPT FILE: $f"
    if gpg -r xiruo.ding@uphs.upenn.edu --encrypt $f; then
	echo "GPG ENCRYPTION SUCCESS!"
    else
	echo "GPG ENCRYPTION FAILED!"
    fi
done

