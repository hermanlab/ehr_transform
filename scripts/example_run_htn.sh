#! /bin/bash

############  QUERY
# ENC
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_enc ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --log2dir query_pds ./SQL/extract_enc.sql

# DEMO
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_demo ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --log2dir query_pds ./SQL/extract_demo.sql

# LAB EPIC
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_lab_epic ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --log2dir query_pds ./SQL/extract_lab_epic.sql
# LAB EPIC resume
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_lab_epic ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --log2dir query_pds ./SQL/extract_lab_epic.sql --resume_run



# LAB CERNER
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_lab_cerner ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --log2dir query_pds ./SQL/extract_lab_cerner.sql --temp_table


# uni_id_mrn log to another file
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_uni_id_mrn ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --logfile ./logs/htn_all/v0.2.3.1/uni_id_mrn_log.txt query_pds ./SQL/extract_uni_id_mrn.sql --full_chunk


# DX
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_dx ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --logfile ./logs/htn_all/v0.2.3.1/dx_log query_pds ./SQL/extract_dx.sql


# MEDS
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_meds ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --logfile ./logs/htn_all/v0.2.3.1/meds_log query_pds ./SQL/extract_meds.sql



# NOTES

python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_notes ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --logfile ./logs/htn_all/v0.2.3.1/notes_log.txt query_notes --chunk_size 1000




########### PROCESS NOTES
python -m cProfile -o ./logs/htn_all/v0.2.3.1/cprofile_regex_notes ./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --logfile ./logs/htn_all/v0.2.3.1/notes_regex_log.txt count_notes --workers 7

########### PROCESS MEDS
./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv --log2dir count_meds --gpg_indir --workers 7




##########  R Merge

# TODO:
# R
./raw_2_enc_pts_level.R v0.2.3.1 --project htn_all --mc_core 6


######### Concatenate to CSV & SQLite

# csv (100min)
./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv concat --indir data/processed_data/htn_all/RAR_v0.2.3.1/enc_level/ --outdir ./data/processed_data/htn_all/RAR_v0.2.3.1/ --outfile enc.csv

./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -vv concat --indir data/processed_data/htn_all/RAR_v0.2.3.1/pts_level/ --outdir ./data/processed_data/htn_all/RAR_v0.2.3.1/ --outfile pts.csv

# SQLite (200min)
./ehr_transform.py --project htn_all --pplversion v0.2.3.1 -v --log2dir concat_sqlite

######### Deidentify Individual CSV's (testing) -- # 76 (since 3 RDB found in this cohort)
./deidentify.R v0.2.3.1 --project htn_all --enc_file ./data/processed_data/htn_all/RAR_v0.2.3.1/enc_level/enc_level_76.csv --pts_file ./data/processed_data/htn_all/RAR_v0.2.3.1/pts_level/pts_level_76.csv