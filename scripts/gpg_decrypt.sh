#! /bin/bash
shopt -s extglob

# Decrypt all *.gpg files in $1
DIRECTORY=$1

if [ ! -d "$DIRECTORY" ]; then
    echo "ERROR: no such directory $DIRECTORY"
    exit
fi
echo "Processing directory: $DIRECTORY"



files=$DIRECTORY/*.gpg

for f in $files; do
    echo "DECRYPT FILE: $f"
    if gpg --output ${f%.gpg} -d $f; then
	echo "GPG DECRYPTION SUCCESS!"
    else
	echo "GPG DECRYPTION FAILED!"
    fi
done
