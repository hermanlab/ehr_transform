import glob
import os
import subprocess

from distutils.core import Command
from setuptools import setup, find_packages

subprocess.call(
    ('mkdir -p EHR_transform/data && '
     'git describe --tags --dirty > EHR_transform/data/ver.tmp '
     '&& mv EHR_transform/data/ver.tmp EHR_transform/data/ver '
     '|| rm -f EHR_transform/data/ver.tmp'),
    shell=True, stderr=open(os.devnull, "w"))

from EHR_transform import __version__


class CheckVersion(Command):
    description = 'Confirm that the stored package version is correct'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        with open('EHR_transform/data/ver') as f:
            stored_version = f.read().strip()

        git_version = subprocess.check_output(
            ['git', 'describe', '--tags', '--dirty']).strip()

        assert stored_version == git_version
        print('the current version is', stored_version)


package_data = ['data/*']

params = {'author': 'Your name',
          'author_email': 'Your email',
          'description': 'Package description',
          'name': 'EHR_transform',
          'packages': find_packages(),
          'package_dir': {'EHR_transform': 'EHR_transform'},
          'entry_points': {
              'console_scripts': ['runme = EHR_transform.scripts.main:main']
          },
          'version': __version__,
          'package_data': {'EHR_transform': package_data},
          'test_suite': 'tests',
          'cmdclass': {'check_version': CheckVersion}
          }

setup(**params)
