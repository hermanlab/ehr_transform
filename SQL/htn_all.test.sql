-- This is the very first file to run to generate the population
-- This is NOT pseudo sql codes. It could be run directly from sql command line

-- htn_all
DROP TABLE RAR_FULL_PTS;
CREATE TABLE RAR_FULL_PTS NOLOGGING AS
SELECT PI.EMPI
from ODS.ENCOUNTER PARTITION (EPIC) E
   LEFT JOIN ODS.R_ENCOUNTER_STATUS RES
    ON E.FK_ENC_STATUS_ID = RES.PK_ENC_STATUS_ID
  JOIN ODS.PATIENT P
    ON E.FK_PATIENT_ID = P.PK_PATIENT_ID
  JOIN ODS.PATIENT_IDENTIFIERS PARTITION (EPIC) PI
    ON P.PK_PATIENT_ID = PI.FK_PATIENT_ID
WHERE E.ENC_DATE >= to_date('01-Jan-2007', 'dd-mon-yyyy') AND E.ENC_DATE < to_date('01-Jan-2018', 'dd-mon-yyyy')
  AND E.PATIENT_MASTER_CLASS = 'OUTPATIENT'
  AND E.BP_DIASTOLIC IS NOT NULL
  AND E.BP_SYSTOLIC IS NOT NULL
  AND PI.EMPI IS NOT NULL
  AND PI.Identifier_Facility = 'HUP'
  AND P.SOURCE_CODE = 'EPIC'
  AND ROWNUM < 100
GROUP BY PI.EMPI
;



ALTER TABLE rar_full_pts
  ADD uni_id INTEGER
;
UPDATE rar_full_pts
    SET uni_id = ROWNUM;

ALTER TABLE rar_full_pts
ADD CONSTRAINT rar_full_pts
PRIMARY KEY (EMPI);

CREATE UNIQUE INDEX idx_pt_id
  ON rar_full_pts (uni_id, EMPI);




