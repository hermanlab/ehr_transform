FROM rocker/tidyverse:3.4.1
MAINTAINER Daniel Herman <daniel.herman2@uphs.upenn.edu>

# Clean up users
RUN groupadd -g 1004 pa
RUN useradd --create-home --shell /bin/bash -u 1003 --gid pa --groups sudo -p $(openssl passwd -1 PL3@5ecH@nG3) hermanda
RUN useradd --create-home --shell /bin/bash -u 1006 --gid pa --groups sudo -p $(openssl passwd -1 PL3@5ecH@nG3) dingxi
RUN deluser rstudio

RUN apt-get update -qq && \
    apt-get -y install \
        nano \
        libpam-pwquality \
        curl \
        bzip2 \
        git

# Setup password requirements
RUN sed -i '25s/.*/password\trequisite\t\t\tpam_pwquality.so retry=3 difok=1 minlen=9 ucredit=-1 lcredit=-1 dcredit=-1 ocredit=-1 enforce_for_root/' /etc/pam.d/common-password


# need to specify versions for libs
RUN install2.r --error \
    --deps FALSE \
    logging \
    cowplot \
    kableExtra \
    ini \
    reporttools \
    gtools \
    geosphere \
    proto \
    findpython \
    getopt \
    rjson \
    argparse \
    randomForest \
    iterators \
    foreach \
    glmnet \
    doParallel \
    doMC \
    corrplot

RUN apt-get install -y locales locales-all
# Not essential, but wise to set the lang
# Note: Users with other languages should set this in their derivative image
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV PYTHONIOENCODING UTF-8

# Install miniconda & python
RUN mkdir /temp && \
    cd /temp && \
    curl -O https://repo.continuum.io/miniconda/Miniconda3-4.3.31-Linux-x86_64.sh
RUN cd /temp/ && \
    bash Miniconda3-4.3.31-Linux-x86_64.sh -b -p /miniconda3 && \
    cd / && \
    rm -rf ../temp
ENV PATH="/miniconda3/bin:${PATH}"
RUN conda install -y -q python=3.5.4

# Install Oracle Instant Client
COPY oracle/instantclient_11_2.zip ./
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y libaio1 && \
    unzip instantclient_11_2.zip && \
    mv instantclient_11_2/ /usr/lib/ && \
    rm instantclient_11_2.zip && \
    ln /usr/lib/instantclient_11_2/libclntsh.so.11.1 /usr/lib/libclntsh.so && \
    ln /usr/lib/instantclient_11_2/libocci.so.11.1 /usr/lib/libocci.so && \
    ln /usr/lib/instantclient_11_2/libociei.so /usr/lib/libociei.so && \
    ln /usr/lib/instantclient_11_2/libnnz11.so /usr/lib/libnnz11.so

ENV ORACLE_BASE /usr/lib/instantclient_11_2
ENV LD_LIBRARY_PATH /usr/lib/instantclient_11_2
ENV TNS_ADMIN /usr/lib/instantclient_11_2
ENV ORACLE_HOME /usr/lib/instantclient_11_2

# Install python packages
RUN conda install -y -q \
    pandas==0.22.0 \
    numpy==1.14.0 \
    matplotlib==2.1.1 \
    scipy==1.0.0 \
    scikit-learn==0.19.1 \
    pymssql==2.1.3 \
    cx_oracle==6.0.3 && \
    pip install python-gnupg==0.4.1 \
    pycrypto==2.6.1

# Install gpg
RUN mkdir /gpg_tmp && \
    cd /gpg_tmp && \
    curl -O https://www.gnupg.org/ftp/gcrypt/gnupg/gnupg-1.4.22.tar.bz2 && \
    tar xvjf gnupg-1.4.22.tar.bz2 && \
    cd gnupg-1.4.22 && \
    ./configure && \
    make && \
    make install && \
    cd / && \
    rm -rf gpg_tmp





