import numpy as np
import pandas as pd
import cx_Oracle
import os
import getpass
import csv

"""Read a password from the console."""
def ask_user_password(prompt):
    return getpass.getpass(prompt + ": ")

def createOracleSQLconnection(username='dingxi', host='uphsvlndc069',port = 1521, sid = 'pdsprd.uphs.upenn.edu'):
    password = ask_user_password("PW")
    connect_string = username + '/' + password + '@' + host + ':' + str(port) + '/' + sid
    connection = cx_Oracle.connect(connect_string)
    # return connection.cursor()
    return connection

def createOracleSQLconnection_readpwd(pwd, username='dingxi', host='uphsvlndc069',port = 1521, sid = 'pdsprd.uphs.upenn.edu'):
    password = pwd
    connect_string = username + '/' + password + '@' + host + ':' + str(port) + '/' + sid
    connection = cx_Oracle.connect(connect_string)
    return connection

def SQLquery2df(query, cursor):
    cursor.execute(query)

    # Get Header #
    cols = []
    if cursor.description is not None:
        for col in cursor.description:
            cols.append(col[0])

    # Get Rows #
    rows = cursor.fetchall()

    # Put into a DataFrame
    df = pd.DataFrame(columns=cols)
    for idx,c in enumerate(cols):
        df[c] = [r[idx] for r in rows]

    return df

def cursor2df(cursor):


    # Get Header #
    cols = []
    if cursor.description is not None:
        for col in cursor.description:
            cols.append(col[0])

    # Get Rows #
    rows = cursor.fetchall()

    # Put into a DataFrame
    df = pd.DataFrame(columns=cols)
    for idx,c in enumerate(cols):
        df[c] = [r[idx] for r in rows]

    return df

def SQL2csv(cursor,sql, file):

    cursor.execute(sql)
    # Get Rows #

    fp = open(file, 'w')
    csvFile = csv.writer(fp)

    csvFile.writerow(col[0] for col in cursor.description)

    rows = cursor.fetchall()
    csvFile.writerows(rows)


    fp.close()

    return len(rows)
