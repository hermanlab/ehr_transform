"""
Assembles subcommands and provides top-level script.
"""

import argparse
from argparse import RawDescriptionHelpFormatter
import logging
import pkgutil
import sys
from importlib import import_module
from EHR_transform import subcommands, __version__ as version, __doc__ as docstring
from EHR_transform.utils import Opener
import configparser
import os

def parse_arguments(argv):
    """
    Create the argument parser
    """

    parser = argparse.ArgumentParser(description=docstring)

    parser.add_argument('-V', '--version', action='version',
                        version=version,
                        help='Print the version number and exit')
    parser.add_argument('-v', '--verbose',
                        action='count', dest='verbosity', default=1,
                        help='Increase verbosity of screen output (eg, -v is verbose, '
                        '-vv more so)')
    parser.add_argument('-q', '--quiet',
                        action='store_const', dest='verbosity', const=0,
                        help='Suppress output')
    parser.add_argument('--logfile', default=sys.stderr,
                        type=Opener('w'), metavar='FILE',
                        help='Write logging messages to FILE [default stderr]')
    parser.add_argument('--config', default='./config.ini',
                        help='Config File. Default is ./config.ini')
    parser.add_argument('--project', default='PA',
                        help='Project name to work on. Default is PA')
    parser.add_argument('--pplversion', default='v0.2.3',
                        help='Pipeline version. Default is v0.2.3')


    parser.add_argument('--log2dir', action='store_true',
                        help='Write logging messages into ./logs DIR.[default]')
    tmp = parser.parse_known_args()
    config = configparser.ConfigParser()
    config.read(tmp[0].config)
    prj = tmp[0].project
    pipeline_version = tmp[0].pplversion

    ##########################
    # Setup all sub-commands #
    ##########################

    subparsers = parser.add_subparsers(dest='subparser_name', title='actions')

    # Begin help sub-command
    parser_help = subparsers.add_parser(
        'help', help='Detailed help for actions using `help <action>`')
    parser_help.add_argument('action', nargs=1)
    # End help sub-command

    # Organize submodules by argv
    modules = [name for _, name, _ in pkgutil.iter_modules(subcommands.__path__)]
    modules = [m for m in modules if not m.startswith('_')]
    run = [name for name in modules if name in argv]

    actions = {}

    # `run` will contain the module corresponding to a single
    # subcommand if provided; otherwise, generate top-level help
    # message from all submodules in `modules`.
    for name in run or modules:
        # set up subcommand help text. The first line of the dosctring
        # in the module is displayed as the help text in the
        # script-level help message (`script -h`). The entire
        # docstring is displayed in the help message for the
        # individual subcommand ((`script action -h`))
        # if no individual subcommand is specified (run_action[False]),
        # a full list of docstrings is displayed
        mod = import_module('{}.{}'.format(subcommands.__name__, name))

        if mod.__doc__.strip():
            helpstr = mod.__doc__.lstrip().split('\n', 1)[0]
        else:
            helpstr = '<add help text in docstring>'

        subparser = subparsers.add_parser(
            name, help=helpstr,
            description=mod.__doc__,
            formatter_class=RawDescriptionHelpFormatter)
        mod.build_parser(subparser,
                         config=config, prj=prj,
                         pipeline_version=pipeline_version)
        actions[name] = mod.action

    # Determine we have called ourself (e.g. "help <action>")
    # Set arguments to display help if parameter is set
    #           *or*
    # Set arguments to perform an action with any specified options.
    arguments = parser.parse_args(argv)
    # Determine which action is in play.
    action = arguments.subparser_name

    # Support help <action> by simply having this function call itself and
    # translate the arguments into something that argparse can work with.
    if action == 'help':
        return parse_arguments([str(arguments.action[0]), '-h'])

    return actions[action], arguments


def main(argv=None):

    if argv is None:
        argv = sys.argv[1:]

    action, arguments = parse_arguments(argv)

    loglevel = {
        0: logging.ERROR,
        1: logging.WARNING,
        2: logging.INFO,
        3: logging.DEBUG,
    }.get(arguments.verbosity, logging.DEBUG)


    logformat ='[%(asctime)s] %(levelname)s %(module)s %(lineno)s %(message)s'

    # specify logging file
    if arguments.log2dir:
        log_path = './logs/' + arguments.project + '/' + arguments.pplversion
        if not os.path.exists(log_path):
            os.makedirs(log_path) ## use os.makedirs to make dir recursively
        logfile_str = log_path + '/' + arguments.subparser_name + '_log.txt'
        logfile = Opener('a')(logfile_str)
    else:
        logfile = arguments.logfile

    logging.basicConfig(stream = logfile, format=logformat, level=loglevel, datefmt= "%Y-%m-%d %H:%M:%S %Z")

    return action(arguments)
