import re
import pandas as pd
import time
import os

def read_notes(dat_file, nrows=None, log=None):
    # read in all columns as object
    notes = pd.read_csv(dat_file, header=0, dtype=object, nrows=nrows)


    notes.NOTE_LINE = notes.NOTE_LINE.astype(int)

    # Format ENTRY_TIME as DateTime
    notes.ENTRY_TIME = pd.to_datetime(notes.ENTRY_TIME, format="%Y-%m-%d %H:%M:%S")

    # Format ENCOUNTER_DATE as DateTime
    notes.ENCOUNTER_DATE = pd.to_datetime(notes.ENCOUNTER_DATE, format="%Y-%m-%d")

    if log:
        log.info("READING IN FILE [%s]" %os.path.basename(dat_file))

    return notes


def notes_cleaning(notes, log=None):
    """
    Clean raw notes. Input file are raw notes, read from read_notes function. The following steps would be performed:
    (1) Concatenate NOTE_LINE
    (2) For each NOTE_ID, pick the last ENTRY_TIME; if all None, keep all
    (3) Remove NOTE_STATUS == "Deleted"
    (4) Pick NOTE_ID by ordered NOTE_STATUS

    @param notes: pandas df of raw note file; read from read_notes function
    @return notes_final: pandas df
    """

    # IMPORTANT! If not drop NaN, join(x) will return an error: expected str, float found!
    notes.dropna(subset=['NOTE_TEXT'], inplace=True)

    # concatenate NOTE_LINE
    empi_0 = notes.EMPI.nunique()
    note_id_0 = notes.NOTE_ID.nunique()

    ## keep basic info in tp
    tp = notes.\
        drop(['NOTE_TEXT', 'NOTE_LINE'], axis=1).\
        drop_duplicates(inplace=False).copy()
    tp.set_index(['CONTACT_SERIAL_NUM'], drop=True, inplace=True)
    
    # keep concatenated NOTES in texts
    texts = notes.\
        sort_values(['NOTE_LINE'], ascending=True).\
        groupby('CONTACT_SERIAL_NUM')\
        ['NOTE_TEXT'].\
        agg(lambda x: " ".join(x)).\
        to_frame()

    # merge tp and texts
    notes_agg = tp.merge(texts, how='left',
                     left_index=True, right_index=True)
    notes_agg.reset_index(drop=False, inplace=True)


    
    # make NaN NOTE_STATUS as "Unknown"
    notes_agg.loc[pd.isnull(notes_agg.NOTE_STATUS), 'NOTE_STATUS'] = 'Unknown'


    empi_1 = notes_agg.EMPI.nunique()
    note_id_1 = notes_agg.NOTE_ID.nunique()
    if log:
        msg = "CONCATENATED NOTE_LINE. EMPI [%d --> %d], NOTE_ID [%d --> %d]" %(empi_0, empi_1, note_id_0, note_id_1)
        log.info(msg)

    # Pick last ENTRY_TIME for each NOTE_ID
    macTime = notes_agg.groupby('NOTE_ID', as_index=False)['ENTRY_TIME'].max()
    macTime = macTime.rename(columns={"ENTRY_TIME":"Max_ENTRY_TIME"})

    notes_agg = notes_agg.merge(macTime, on='NOTE_ID')

    idx = pd.isnull(notes_agg.Max_ENTRY_TIME) | (pd.notnull(notes_agg.Max_ENTRY_TIME) & (notes_agg.Max_ENTRY_TIME == notes_agg.ENTRY_TIME))

    notes_agg = notes_agg[idx]

    notes_agg = notes_agg.drop(['Max_ENTRY_TIME'], axis=1)


    
    # QC: for step of picking last ENTRY_TIME, any NOTE_ID missed?
    empi_2 = notes_agg.EMPI.nunique()
    note_id_2 = notes_agg.NOTE_ID.nunique()
    if log:
        msg = "PICKED BY LAST ENTRY_TIME. EMPI [%d --> %d], NOTE_ID [%d --> %d]" %(empi_1, empi_2, note_id_1, note_id_2)
        log.info(msg)


    # make final notes: (1) remove "Deleted" (2) pick based on order

    ## Addendum > Cosigned > Signed >  Cosign Needed Addendum > Finalized > Revised > Cosign Needed > Shared > Unsigned > Incomplete Revision > Incomplete > Unknown
    ## Note: For unseen categories, assign them to "Unknown"


    notes_agg = notes_agg[notes_agg.NOTE_STATUS != 'Deleted']

    empi_3 = notes_agg.EMPI.nunique()
    note_id_3 = notes_agg.NOTE_ID.nunique()
    if log:
        msg = "REMOVED DELETED NOTES. EMPI [%d --> %d], NOTE_ID [%d --> %d]" %(empi_2, empi_3, note_id_2, note_id_3)
        log.info(msg)


    ## make NOTE_STATUS catigorical
    notes_agg['NOTE_STATUS'] = pd.Categorical(notes_agg['NOTE_STATUS'],
                                                categories=['Addendum', 'Cosigned',
                                                            'Signed' ,  'Cosign Needed Addendum' , 'Finalized' , 'Revised' , 'Cosign Needed',
                                                            'Shared', 'Unsigned','Incomplete Revision', 'Incomplete', 'Unknown'],
                                                ordered=True).fillna('Unknown')


    ## select based on order:
    notes_agg = notes_agg.sort_values('NOTE_STATUS', ascending=True, na_position='last').groupby('NOTE_ID').head(1)


    empi_4 = notes_agg.EMPI.nunique()
    note_id_4 = notes_agg.NOTE_ID.nunique()
    if log:
        msg = "NOTE CLEANING FINISHED! EMPI [%d --> %d], NOTE_ID [%d --> %d]" %(empi_0, empi_4, note_id_0, note_id_4)
        log.info(msg)

    return notes_agg

def count_regex(note, re_compiled):
    """
    Coumt the number of observations of compilted regex re_compiled in note

    @param note: This is string of a text notes
    @param re_compiled: This is a compiled regular expression
    @return: Integer number of matches for eac
    """
    i = 0

    for note_split in note.split(r"."):
        # trim multiple spaces into 1
        note_split = re.sub(r"\s+", " ", note_split)
        if (re_compiled.search(note_split) is not None):
            i += 1

    return i


def add_regex_counts(notes_df, regex_dict,
                     cols_list=list(['CONTACT_SERIAL_NUM', 'EMPI', 'PAT_ENC_CSN_ID',
                                     'ENCOUNTER_DATE', 'NOTE_ID', 'NOTE_STATUS',
                                     'NOTE_TYPE', 'NOTE_TEXT']),
                     log=None):
    """
    Count the number of observations of a set of regex in a series of notes

    @param notes_df: pandas df
    @param regex_dict: dictionary of regex strings
    @param cols_list: list of column names to keep
    @return: pandas df
    """

    for key, value in regex_dict.items():

        re_comp = re.compile(value, re.IGNORECASE)

        if key == 'word_count':
            notes_df["re_" + key] = notes_df.NOTE_TEXT.apply(lambda x:
                                                             len(re_comp.findall(x)))
        else:
            notes_df["re_"+ key] = notes_df.NOTE_TEXT.apply(count_regex,
                                                 re_compiled = re_comp)

        # select Identifier Columns and RE_ columns to keep
    cols_list = [x
               for x in cols_list
               if x in list(notes_df.columns)]
    r = re.compile(r"^re_")
    cols_list.extend(list(filter(r.match, list(notes_df.columns))))
    notes_df = notes_df[cols_list]

    return notes_df



def display_regex(note, re_compiled):

    for note_split in note.split(r"."):
        note_split = re.sub(r"\s+", " ", note_split)
        note_split_match = re_compiled.search(note_split)
        if note_split_match is not None:
            # highlight regex hits: 34-blue, 1-Bold, r'\g<0>' - whole match
            print('\033[95m\033[1m' + "NEWLINE  " + '\033[0m' +re_compiled.sub('\033[34m\033[1m' +
                                                                               r'\g<0>' +
                                                                               '\033[0m', note_split))


def display_regex_hits(notes_df, regex_dict,
                       pattern_key):

    assert (pattern_key in regex_dict), "Regex_key is Undefined!"

    re_comp = re.compile(regex_dict[pattern_key], re.IGNORECASE)
    notes_df.NOTE_TEXT.apply(display_regex,
                             re_compiled=re_comp)



import getpass
"""Read a password from the console."""
def ask_user_password(prompt):
    return getpass.getpass(prompt + ": ")

import pymssql
def createMSSQLconnection(username='luongth', host='ClarityDev', database='clarity_snapshot_db', domain='UPHS'):
    password = ask_user_password("PW")
    connection = pymssql.connect(
            host=host,
            database=database,
            user=domain+'\\'+username,
            password=password
    )
    return connection.cursor()

def SQLquery2df(query, cursor):
    cursor.execute(query)

    # Get Header #
    cols = []
    if cursor.description is not None:
        for col in cursor.description:
            cols.append(col[0])

    # Get Rows #
    rows = cursor.fetchall()

    # Put into a DataFrame
    df = pd.DataFrame(columns=cols)
    for idx,c in enumerate(cols):
        df[c] = [r[idx] for r in rows]

    return df


def med_agg(x, grp_nm="Default"):
    from collections import OrderedDict

    if grp_nm == "Default":
        names = OrderedDict([
            ('MED_N', x['MED_NAME'].count()),
            ("MED_UNIQUE_N", x['MED_NAME'].nunique())
        ])
    else:
        names = OrderedDict([
            ('MED_' + grp_nm + '_N', x['MED_NAME'].count()),
            ('MED_' + grp_nm + '_UNIQUE_N', x['MED_NAME'].nunique())
        ])


    return pd.Series(names)


def collapse_note_empi_date(df, regex_dict, log=None):
    #' Read in note_regex and collapse into EMPI_DATE LEVEL

    if log:
        log.info("START TO COLLAPSE INTO EMPI DATE.")
    df['EMPI_DATE'] = df['EMPI'] + " " + df['ENCOUNTER_DATE']

    # for logging purpose
    empi_date_0 = df.EMPI_DATE.nunique()
    empi_0 = df.EMPI.nunique()

    re_cols = ["re_" + x for x in list(regex_dict.keys())]

    df[re_cols] = df[re_cols].apply(pd.to_numeric)

    # select re_cols and EMPI_DATE only
    df = df[['EMPI_DATE'] + re_cols]

    df_sum = df.groupby('EMPI_DATE').sum()        # dataframe for sums

    df_count = df.groupby('EMPI_DATE').size().to_frame('NOTE_n')        # dataframe for NOTE Count

    df_tmp_ret = df_sum.merge(df_count, how='outer',
                              left_index=True, right_index=True).reset_index()    # merge two dataframes


    empi_date_1 = df_tmp_ret.EMPI_DATE.nunique()
    empi_1 = df_tmp_ret.EMPI_DATE.str.slice(0, 10).nunique()

    if log:
        log.info("COLLAPSING NOTE FILE FINISHED [EMPI: %d -> %d; EMPI_DATE: %d -> %d]"
                 %(empi_0, empi_1, empi_date_0, empi_date_1))

    return df_tmp_ret



def empi_to_pts_id(df, empi_pts_id, pts_id, log=None):
    #' replace EMPI_DATE in df into ID_DATE, and UNI_ID will be added
    #' @param df input dataframe
    #' @param empi_pts_id EMPI-PTS_ID dataframe
    #' @param pts_id Patient ID, such as EMPI, UNI_ID
    #' @return ret dataframe

    if pts_id not in empi_pts_id.columns:
        if log:
            log.error("SPECIFIED pts_id [%s] NOT FOUND IN EMPI-UNIID FILE [%s]" %(pts_id, os.path.basename(empi_pts_id_file)))
        sys.exit(1)

    df['EMPI'] = df['EMPI_DATE'].str.slice(0,10)
    df['DATE'] = df['EMPI_DATE'].str.slice(11)
    ret = df.merge(empi_pts_id, how='inner')
    ret['ID_DATE'] = ret[pts_id] + " " + ret["DATE"]
    
    ret = ret.drop(['EMPI', 'EMPI_DATE', 'DATE'], axis=1)
    return ret


def deidentify(df, pts_id, mapping_table, drop_cols=[], dt_cols=[]):
    #' this is deidentification function that is used to shift dates according to the generated mapping file
    #' the functionality is similar to the R version of deidentify_by_type function
    #' However, this function will turn dates into seconds from 1970-01-01 00:00:00, in compatible with dates generated in R

    ret = df.drop(drop_cols, axis=1)
    
    ret = df.merge(mapping_table, on=pts_id, how='left')
   
    for i in dt_cols:
        ret[i] = (ret[i] - pd.Timestamp("1970-01-01")).astype('timedelta64[s]')

    ret = ret.drop(['shift'], axis=1)
    return ret


