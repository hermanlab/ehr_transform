from collections import OrderedDict
regex_dict = { 'hyperaldo': r"hyperaldo",
               'hyperaldo_spec': r"(?<!not\s)(?<!secondary\s)(?<!ruled out\s)(?<!ruling out\s)hyperaldo",
               'primary_hyperaldo_spec': r"(?<!not\s)(?<!secondary\s)(?<!ruled out\s)(?<!ruling out\s)primary hyperaldo",
               'primary_aldo': r"primary aldo",
               'primary_aldo_spec': r"(?<!not\s)(?<!rule out\s)(?<!n't suggest\s)primary aldo",
               'avs': r"adrenal vein sampl|\bavs\b",
               'bah': r"bilateral adrenal hyperplasia|\bbah\b",
               'bah_spec': r"(?<!not\s)bilateral adrenal hyperplasia|(?<!not\s)\bbah\b",
               'aldo_producing_adenoma': r"aldo\w{0,7}([-\s]producing)?([-\s]ade)?noma",
               'aldo_producing_adenoma_spec': r"(?<!not\s|nor\s)aldo\w{0,7}([-\s]producing)?([-\s]ade)?noma",
               'adrenal_adenoma': r"adrenal adenoma",
               'htn': r"(?<!pulmonary\s)(?<!pulmonary arterial\s)(htn\b|hypertension)",
               'htn_spec': r"(?<!pulmonary\s)(?<!pulmonary arterial\s)(?<!not\s|nor\s)(htn\b|hypertension)",
               'htn_teixera':  r"(?!pulm\w*\W*\w+\W+)\b(htn|hypertension)",
               'relative_hyperaldo': r"relative (hyper)?aldo",
               'relative_hyperaldo_spec': r"(?<!not\s|nor\s)relative (hyper)?aldo",
               'salt_sensit': r"salt sensiti",
               'salt_sensit_spec': r"(?<!not\s|nor\s)(?<!not indicate )(?<!not consistent with hyperaldosteronism or )(?<!not show )salt sensiti",
               'adrenalectomy': r"adrenalectomy",
               'adrenalectomy_spec': r"(?<!not\s|nor\s)adrenalectomy",
               'word_count': r"\b\w+\b"
}
regex_dict = OrderedDict(sorted(regex_dict.items(), key=lambda t: t[0]))
