"""
Cipher for ID's
"""

# This script is used to decipher all kinds of ID's (except UNI_ID), like PK_ENCOUNTER_ID, ORDER_ID, PROVIDER_ID, etc
# Also, for demo, birthdates of patients whose age was >= 90 years old at 2018-01-01 will be all moved to 90 years ago from 2018-01-01, so that no elder patients could be identified by age
# Also, for demo, ZIP_distance_km was categorized into ZIP_CAT

# NOTE for ENV:
# Note: a file named ".cipher_key", which stores 16-digit key for AES cipher, is REQUIRED!
import pandas as pd
import numpy as np
import sys
import os
import logging
from datetime import datetime, timedelta
import sqlite3
from Crypto.Cipher import AES




log = logging.getLogger(__name__)


def build_parser(parser, config, prj, pipeline_version):

    infile = os.path.join(config['Directories']['repos_dir'],
                         config['Directories']['raw_data_temp_dir'],
                         config[prj]['prj_dir'], "RAR_"+pipeline_version,
                          "EMPI_UNIID.csv")


    source_dir = os.path.join((config['Directories']['repos_dir']),
                          config['Directories']['processed_data_temp_dir'],
                          config[prj]['prj_dir'], "RAR_"+pipeline_version)
    chunk_size = config[prj]['chunk_size']

    parser.add_argument('tb',
                        type=str,
                        help='DB table to cipher')

    source_db = source_dir + "/" + prj + "_pk_enc_DEID.sqlite"
    parser.add_argument('--source_DB',
                        type=str,
                        default=source_db,
                        help='source DB to cipher')

    output_db = source_dir + "/" + prj + "_DEID_CIPHER.sqlite"
    parser.add_argument('--output_DB',
                        type=str,
                        default=output_db,
                        help='output DB')

    parser.add_argument('--chunksize_num',
                        type=int,
                        default=10000,
                        help='chunk size to process each time; irrelevant to chunk size in query pds')



def action(args):

    with open(".cipher_key") as f:
        cipher_key = f.read().splitlines()[0]

    db_in = args.source_DB
    tb = args.tb
    db_out = args.output_DB

    # CBC mode: encryption cipher book is dependent on previous encryption...
    # ECB mode: encryption cipher book is independent on previous encryption... so we could identify same ID by encrypted value
    # <----- need go to secret file! not memory...
    obj=AES.new(cipher_key, AES.MODE_ECB)



    cols_drop = []
    if tb == 'dx':

        cols = ['PK_DX_ID', "PK_ENCOUNTER_ID"]
        cols_drop = ['COMMENTS']
    elif tb == 'encounters':

        cols = ["PK_ENCOUNTER_ID", "HAR_NUMBER", "FK_ADMITTING_PROVIDER_ID", "FK_ATTENDING_PROVIDER_ID"]
    elif tb == 'lab':

        cols = ['PK_ENCOUNTER_ID', "HAR_NUMBER", "PK_ORDER_ID", "PK_ORDER_RESULT_ID", "ORDER_PROVIDER_ID"]
        cols_drop = ['RESULT_TEXT']
    elif tb == 'med':

        cols = ['PK_ENCOUNTER_ID',"PK_ORDER_ID", "ORDER_PROVIDER_ID", "ORDER_ENTRY_PROV_ID"]
    elif tb == 'vitals':
        cols = ['PK_ENC_VITAL_SIGN_ID', 'PK_ENCOUNTER_ID']
        cols_drop = ['VITAL_NOTE']
    elif tb == 'demo':
        cols = []
        old_age_to_2018_secs = datetime.strptime('2018-01-01 00:00:00', '%Y-%m-%d %H:%M:%S') - timedelta(days = 365.25*90) - datetime.strptime('1970-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
    elif tb == 'icd_dict':
        cols = []
    elif tb == 'notes':
        cols = ['NOTE_ID']
        cols_drop = ['CONTACT_SERIAL_NUM', 'PAT_ENC_CSN_ID']
    elif tb == 'P_locs_censor_date':
        cols = []
    elif tb == 'P_med_htn_consolidated':
        cols = []
    elif tb == 'P_pts_primary_loc':
        cols = []
    elif tb == 'P_pts_resistant_htn':
        cols = ['PK_ENCOUNTER_ID']
    elif tb == 'P_rar_avs_test_result_code':
        cols = []
    else:
        sys.exit('EXIT ON ERROR: TABLE [{}] NOT FOUND!'.format(tb))



    # query source DB
    conn = sqlite3.connect(db_in)
    conn_new = sqlite3.connect(db_out)
    query_original = "SELECT * FROM {}".format(tb)
    chunksize_num = args.chunksize_num
    chunks = pd.read_sql_query(query_original, conn, chunksize=chunksize_num)

    i = 1
    for chunk in chunks:


        for col in cols:
            col_new_name = col + "_cipher"

            # according to AES, text should be a multiple of 16, such as 16, 32, 48...
            # so for some ID's less than 16 digit (in system, 11-12 are maximum digits), character "X" will be used to fill to the left
            chunk[col_new_name] = chunk[col].fillna("X").apply(lambda x: x.rjust(16, 'X'))
            chunk[col_new_name] = chunk[col_new_name].apply(lambda x: obj.encrypt(x))
            chunk.drop(col, axis=1, inplace=True)

        if len(cols_drop):
            chunk.drop(cols_drop, axis=1, inplace=True)

        if tb == 'demo':
            chunk.BIRTH_DATE = chunk.BIRTH_DATE.apply(lambda x: x if x > old_age_to_2018_secs.total_seconds() else old_age_to_2018_secs.total_seconds())

            # Categorize ZIP_dist: after splitting, there are about 10%-15% in each category, except for last one, only 5%
            chunk['ZIP_CAT'] = pd.cut(chunk.ZIP_distance_km, bins=[0,5,10,20,30,50,100,float("inf")], include_lowest=True,
                          labels=['ZIP_0_5', 'ZIP_5_10', 'ZIP_10_20', 'ZIP_20_30', 'ZIP_30_50', 'ZIP_50_100', 'ZIP_100_Inf'])
            chunk.drop('ZIP_distance_km', axis=1, inplace=True)


        chunk.to_sql(tb, con=conn_new, if_exists='append', index=False)


        print("Chunk # {} [RECORDS PER CHUNK: {}]".format(i, chunksize_num))
        i = i + 1


    # Create Indexes for tables
    if tb == "encounters":
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_enc ON ENCOUNTERS(PK_ENCOUNTER_ID_cipher)")
        conn_new.execute("CREATE INDEX enc_id_date ON ENCOUNTERS(UNI_ID, ENC_DATE)")
        conn_new.execute("CREATE INDEX enc_loc_date ON ENCOUNTERS(MASTER_LOCATION_CODE, SERVICE_MASTER_DESCRIPTION,ENC_DATE)")
        conn_new.execute("CREATE INDEX enc_type ON ENCOUNTERS(ENC_TYPE_MASTER_CODE)")
    elif tb == "dx":
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_dx ON DX(PK_DX_ID_cipher)")
        conn_new.execute("CREATE INDEX dx_pk_enc_id ON DX(PK_ENCOUNTER_ID_cipher)")
        conn_new.execute("CREATE INDEX dx_uni_id ON DX(UNI_ID)")
        conn_new.execute("CREATE INDEX dx_code ON DX(CODE, CODE_STANDARD_NAME)")
    elif tb == 'lab':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_lab ON LAB(PK_ORDER_ID_cipher, RESULT_ITEM_CODE)")
        conn_new.execute("CREATE INDEX lab_pk_enc_id ON LAB(PK_ENCOUNTER_ID_cipher)")
        conn_new.execute("CREATE INDEX lab_RIC ON LAB(RESULT_ITEM_CODE)")
        conn_new.execute("CREATE INDEX lab_uni_id ON LAB(UNI_ID)")
    elif tb == 'med':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_med ON MED(PK_ORDER_ID_cipher)")
        conn_new.execute("CREATE INDEX med_pts_med ON MED(UNI_ID, ENC_DATE, MED_NAME)")
        conn_new.execute("CREATE INDEX med_pts_med_period ON MED(UNI_ID, ORDER_START_DATE, ORDER_STOP_DATE, MED_NAME)")
        conn_new.execute("CREATE INDEX med_med ON MED(MED_NAME)")
        conn_new.execute("CREATE INDEX med_SGN ON MED(SIMPLE_GENERIC_NAME)")
        conn_new.execute("CREATE INDEX med_FN ON MED(FULL_NAME)")
    elif tb == 'vitals':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_vitals ON VITALS(PK_ENC_VITAL_SIGN_ID_cipher)")
        conn_new.execute("CREATE INDEX vitals_pts_vitals ON VITALS(UNI_ID, ENC_DATE, PK_VITAL_SIGN_ID)")
    elif tb == 'demo':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_demo ON DEMO(UNI_ID)")
    elif tb == 'icd_dict':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_icd_dict ON ICD_DICT(Dx_h1, Dx_h2, CODE, CODE_STANDARD_NAME)")
        conn_new.execute("CREATE INDEX icd_dict_h0 ON ICD_DICT(Dx_h0)")
        conn_new.execute("CREATE INDEX icd_dict_h2 ON ICD_DICT(Dx_h2, CODE, CODE_STANDARD_NAME)")
    elif tb == 'notes':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_notes on NOTES(NOTE_ID_cipher)")
        conn_new.execute("CREATE INDEX idx_UNI_ID_NOTE_DT on NOTES(UNI_ID, ENCOUNTER_DATE)")
    elif tb == 'P_locs_censor_date':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_P_locs_censor_date on P_locs_censor_date(MASTER_LOCATION_CODE, SERVICE_MASTER_DESCRIPTION)")
    elif tb == 'P_med_htn_consolidated':
        conn_new.execute("CREATE INDEX P_med_htn_consolidated_dt on P_med_htn_consolidated(UNI_ID, ORDER_START_DATE, ORDER_STOP_DATE)")
    elif tb == 'P_pts_primary_loc':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_P_pts_primary_loc on P_pts_primary_loc(MASTER_LOCATION_CODE, SERVICE_MASTER_DESCRIPTION, UNI_ID)")
    elif tb == 'P_pts_resistant_htn':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_P_pts_resistant_htn on P_pts_resistant_htn(UNI_ID, HTN_Case)")
        conn_new.execute("CREATE INDEX P_pts_resistant_htn_pts_res_dt ON P_pts_resistant_htn(UNI_ID, HTN_Case, DT)")
    elif tb == 'P_rar_avs_test_result_code':
        conn_new.execute("CREATE UNIQUE INDEX unique_idx_P_rar_avs_test_result_code on P_rar_avs_test_result_code(RESULT_ITEM_CODE)")

    conn.close()
    conn_new.close()
