"""
Do query on Clarity_Dev (default) for notes of  a list of Pts (specified in csv file).

OUTPUT: note_1.csv, note_2.csv ... in ./data/notes_queried folder by default

"""

import logging
import time
import pandas as pd
import numpy as np
import os
from EHR_transform.note_fx import createMSSQLconnection, SQLquery2df


log = logging.getLogger(__name__)


def build_parser(parser, config, prj, pipeline_version):

    infile = os.path.join(config['Directories']['repos_dir'],
                         config['Directories']['raw_data_temp_dir'],
                         config[prj]['prj_dir'], "RAR_"+pipeline_version,
                          "EMPI_UNIID.csv")


    outdir = os.path.join(config['Directories']['repos_dir'],
                         config['Directories']['raw_data_temp_dir'],
                         config[prj]['prj_dir'], "RAR_"+pipeline_version,
                         "notes_queried")
    chunk_size = config[prj]['chunk_size']

    parser.add_argument('--infile',
                        type=str,
                        default=infile,
                        help='UNI_ID_MRN.csv file for the cohort. NOTE that only 1 file will be used! A column named EMPI is required. Default is ./data/raw_data/PROJECT_NAME/RAR_Version/EMPI_UNIID.csv')
    parser.add_argument('--sqlfile',
                        default='./SQL/sql_notes.sql',
                        type=str,
                        help='sql code file to exec query. plain text. default file is ./SQL/sql_notes.sql')
    parser.add_argument('--outdir',
                        type=str,
                        default=outdir,
                        help='output directory for storing queried data files. Default is ./data/notes_queried folder')
    parser.add_argument('--db_usr',
                        type=str, default='dingxi',
                        help='Clarity DB user name. Default is dingxi')
    parser.add_argument('--chunk_size', '-chunk',
                        type=int,
                        help="Number of EMPIs to process at once",
                        default = chunk_size)
    parser.add_argument("--resume_run",
                        action="store_true",
                        help="If specified, will only execute SQL chunk where corresponding csv file does not exist")
    parser.add_argument('--test',
                        action='store_true',
                        help='Whether to enter Test mode - only query 100 Pts from group 1')


def action(args):

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # Read in EMPI list
    empi_mrn = pd.read_csv(args.infile, dtype=object)

    empi_uniid = pd.read_csv(args.infile, dtype=object)
    empi_uniid.UNI_ID = empi_uniid.UNI_ID.astype(int)
    empi_uniid.drop_duplicates(inplace=True)
    log.info( "From [%s] file, %d unique EMPI's found for query" %(args.infile,empi_uniid.EMPI.nunique()))

    n_uniid = empi_uniid.UNI_ID.nunique()

    ## construct EMPI lists according to UNI_ID's, in correspondance with PDS query
    chunks = [(x, x+ args.chunk_size)
            for x in range(1, n_uniid + 1, args.chunk_size)]

    empi_chunks = []
    for x in chunks:
        empi_chunks.append(empi_uniid[(empi_uniid.UNI_ID >= x[0]) & (empi_uniid.UNI_ID < x[1])].EMPI)

    log.info("QUERY FOR [NOTES] STARTED!")
    log.info("%s UNI_ID's; %s chunks in total, of size %s each" %(n_uniid,len(chunks), args.chunk_size))

    # Read in SQL base codes
    with open(args.sqlfile) as f:
        sql_raw = f.read()

    # Looping through EMPI chunks to query
    ## Start Clarity_Dev connection
    connection = createMSSQLconnection(username=args.db_usr)

    if args.test:              # in test mode, only query for 100 patients
        empi_chunks = [  empi_chunks[0][:100]  ]

    for i in range(len(empi_chunks)):
        empi_ls = empi_chunks[i]

        ## construct sql text containing list of EMPI's
        sql_chunk = sql_raw + "(" + ",".join("'" + item + "'"
                                             for item in empi_ls) + ")"

        ## file loc and name
        outfile_path = os.path.join(args.outdir, "note_" +  str(i+1) + ".csv")

        ## if resume_run & file exists, continue
        if args.resume_run:
            if os.path.exists(outfile_path) or os.path.exists(outfile_path+'.gpg'):
                continue

        ## query
        note_chunk = SQLquery2df(sql_chunk, connection)

        ## Checking
        if (len(note_chunk) == 0):           # Log empty query results
            log.debug("sql_chunk to query (empty df: %s): %s" %
                      (str(len(note_chunk==0)), sql_chunk))

        ## Write out
        note_chunk.to_csv(outfile_path, index=False)
        log.info("NOTE CHUNK #%d IS CREATED." %(i+1))

    log.info("NOTE QUERY FINISHED!")
    connection.close()
