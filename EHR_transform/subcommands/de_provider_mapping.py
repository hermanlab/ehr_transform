"""
For all providers shown in lab data, assign a unique DE_ID_PROV for them
OUTPUT: de_prov_mapping.csv

"""

import logging
import time
import pandas as pd
import numpy as np
import os
from EHR_transform.note_fx import createMSSQLconnection, SQLquery2df


log = logging.getLogger(__name__)


def build_parser(parser, config, prj, pipeline_version):

    indir = os.path.join(config['Directories']['repos_dir'],
                         config['Directories']['raw_data_temp_dir'],
                         config[prj]['prj_dir'], "RAR_"+pipeline_version,
                         "LAB_EPIC")


    outdir = os.path.join((config['Directories']['repos_dir']),
                          config['Directories']['processed_data_temp_dir'],
                          config[prj]['prj_dir'], "RAR_"+pipeline_version,
                          "mapping_files")

    seed = int(config[pipeline_version]['seed'])

    parser.add_argument('--indir',
                        type=str,
                        default=indir,
                        help='dir for files containing PROVIDER info. Default will be from LAB_EPIC')
    parser.add_argument('--outdir',
                        type=str,
                        default=outdir,
                        help='output directory for mapping file. Default is ./data/mapping_files/')
    parser.add_argument('--seed', 
                        type=int,
                        default=seed,
                        help="Seed for generating random integers")
    parser.add_argument('--test',
                        action='store_true',
                        help='Enter test mode, reading in only 1000 rows for 3 files (if many)')



def action(args):

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)
    
    outfile = os.path.join(args.outdir, "de_prov_mapping.csv")

     # get a list of files with name and full path, in the inputd directory
    file_ls = list()
    for root, dirs, files in os.walk(args.indir):
        for name in files:
            # ignore gpg files
            if os.path.splitext(name)[1] == '.gpg':
                    continue

            full_name = os.path.join(root, name)
            file_ls.append(full_name)

    prov = None

    if args.test:
        file_ls = file_ls[0:5]

    for file_i in file_ls:
        lab_epic = pd.read_csv(file_i, dtype=object)
        tmp = lab_epic.ORDERING_PROV.dropna().unique()

        if prov is None:
            prov = tmp
        else:
            prov = np.concatenate([prov, tmp])

        log.info("PROVIDERS IN LAB_EPIC READ SUCCESSFUL! [%s]" %os.path.basename(file_i))
    del lab_epic, tmp

    prov = pd.unique(prov)
    prov_df = pd.DataFrame({'PROVIDER': prov})

    np.random.seed(args.seed)
    prov_df["DE_ID_PROV"] = np.random.choice(np.arange(1, len(prov_df)+1), size=len(prov_df), replace=False)

    prov_df.to_csv(outfile, index=False)
    log.info("PROVIDERS DEIDENTIFIED MAPPING FILE CREATED! [%s]" %os.path.basename(outfile))

