"""
Do query on PDS, apply the criteria to get the whole cohort of patients

OUTPUT: RAR_FULL_PTS.csv in ./data/raw_data/PROJECT/RAR_Version by default

"""

import logging
import time
import pandas as pd
import numpy as np
import os
import cx_Oracle
import random
import sys
import getpass

from EHR_transform.pds_fx import SQLquery2df, SQL2csv, createOracleSQLconnection_readpwd


log = logging.getLogger(__name__)


def build_parser(parser, config, prj, pipeline_version):

    infile = os.path.join(config['Directories']['repos_dir'],
                         config['Directories']['raw_data_temp_dir'],
                         config[prj]['prj_dir'], "RAR_"+pipeline_version,
                          "EMPI_UNIID.csv")

    outdir = os.path.join(config['Directories']['repos_dir'],
                         config['Directories']['raw_data_temp_dir'],
                         config[prj]['prj_dir'], "RAR_"+pipeline_version)

    chunk_size = config[prj]['chunk_size']

    parser.add_argument('sqlfile',
                        type=str,
                        help='sql code file to exec query. plain text. ')
    parser.add_argument('--infile',
                        type=str,
                        default=infile,
                        help='UNI_ID_MRN.csv file for the cohort. NOTE that only 1 file will be used! A column named EMPI is required. Default is ./data/raw_data/PROJECT_NAME/RAR_Version/EMPI_UNIID.csv')
    parser.add_argument('--outdir',
                        type=str,
                        default=outdir,
                        help='output directory for storing queried data files. Default is ./data/notes_queried folder')
    parser.add_argument('--db_usr',
                        type=str, default='dingxi',
                        help='Clarity DB user name. Default is dingxi')
    parser.add_argument('--chunk_size', '-chunk',
                        type=int,
                        default=chunk_size,
                        help="Number of EMPIs to process at once")
    parser.add_argument('--full_chunk',
                        action='store_true',
                        help='If specified, will not do chunking query')
    parser.add_argument('--temp_table',
                        action='store_true')
    parser.add_argument("--resume_run",
                        action="store_true",
                        help="If specified, will only execute SQL chunk where corresponding csv file does not exist")
    parser.add_argument('--test',
                        action='store_true',
                        help='Whether to enter Test mode - only query 100 Pts from group 1')
    parser.add_argument("--seed",
                    type=int,
                    help="seed for generating randint for selecting a random chunk and a random start point")

def action(args):
    # TYPE and FILENAME
    act = os.path.splitext(os.path.basename(args.sqlfile).replace("extract_", ""))[0].upper()
    filename_base =  "RAR_PTS_" + act


    # modify outdir; make a folder for each TYPE of data
    outdir = os.path.join(args.outdir, act)

    if not os.path.exists(outdir):
        os.makedirs(outdir)


    with open(args.sqlfile) as f:
        sql_raw = f.read()

    pwd = getpass.getpass("PDS PASSWORD:")

    # establish connection
    try:
        connection = createOracleSQLconnection_readpwd( pwd = pwd, username=args.db_usr)
        cursor = connection.cursor()
    except cx_Oracle.DatabaseError as e:
        log.error("DB CONNECTION ERROR: %s" %e)
        sys.exit(1)
    except Exception as e:
        log.error("UNEXPECTED ERROR: %s" %e)
        sys.exit(1)


     # Read in EMPI list
    empi_mrn = pd.read_csv(args.infile, dtype=object)

    empi_uniid = pd.read_csv(args.infile, dtype=object)
    empi_uniid.UNI_ID = empi_uniid.UNI_ID.astype(int)
    empi_uniid.drop_duplicates(inplace=True)

    n_uniid = empi_uniid.UNI_ID.nunique()

    if args.full_chunk:
        chunk_size = n_uniid  # query the whole cohort
    else:
        chunk_size = args.chunk_size

    chunks = [(x, x+ chunk_size)
            for x in range(1, n_uniid + 1, chunk_size)]

    random.seed(1056)
    if args.test:
        ck_i = random.randint(0, len(chunks)-1)
        chunks = chunks[ck_i:ck_i+1]
        test_size = 200

        start_i = random.randint(1, chunk_size-test_size)
        chunks[0] = (start_i, start_i+test_size)


    log.info("QUERY FOR [%s] STARTED!" % act)
    log.info("%s UNI_ID's; %s chunks in total, of size %s each" %(n_uniid,len(chunks), chunk_size))

    i = 0
    for chunk_i in chunks:
        ## construct sql text containing list of EMPI's
        ## in test mode, only query for 200 patients



        if args.temp_table:
             # create a temp EMPI table
            tmp_table = "TMP_EMPI_" + act + "_" + str(i)


            try:
                cursor.execute("DROP TABLE %s"%tmp_table)
            except Exception:
                pass

            try:
                cursor.execute('''
                CREATE TABLE {0} (
                uni_id INTEGER UNIQUE NOT NULL,
                EMPI CHAR(10) PRIMARY KEY
                )
                '''.format(tmp_table))
                connection.commit()

                sql_tmp_empi = '''
                INSERT INTO {0} (uni_id, EMPI)
                SELECT
                uni_id,
                EMPI
                FROM RAR_FULL_PTS
                WHERE UNI_ID >= {1} AND UNI_ID < {2}
                '''.format(tmp_table, chunk_i[0], chunk_i[1])
                cursor.execute(sql_tmp_empi)
                connection.commit()
            except cx_Oracle.DatabaseError as e:
                log.error("DB EXCEPTION: %s" %e)
                sys.exit(1)
            except NameError as e:
                log.error("DB EXCUTION NAME ERROR: %s" %e)
                sys.exit(1)
            except Exception as e:
                log.error("UNEXPECTED ERROR: %s" %e)
                sys.exit(1)


            sql_run = sql_raw.format(tmp_table)
        else:

            sql_tmp = sql_raw.format('''
            (SELECT UNI_ID, EMPI FROM RAR_FULL_PTS
            WHERE UNI_ID >= {0} AND UNI_ID < {1})
            ''')
            sql_run = sql_tmp.format(chunk_i[0], chunk_i[1])


        ## file loc and name
        i += 1
        nm = os.path.join(outdir, filename_base + "_" +  str(i) + ".csv")

        # In resume_run mode, if the file does not exist, SQL will be executed and csv will be created
        # Otherwise, csv files would be overwritten with new files queried below
        if args.resume_run:
            if os.path.exists(nm) or os.path.exists(nm+'.gpg'):
                continue


        # result_chunk = cursor2df(connection)    # query
        try:
            n_row = SQL2csv(cursor, sql_run, nm)
        except cx_Oracle.DatabaseError as e:
            log.error("DB EXCEPTION: %s" %e)
            sys.exit(1)
        except Exception as e:
            log.error("UNEXPECTED ERROR: %s" %e)
            sys.exit(1)


        if args.temp_table:
            try:
                cursor.execute("DROP TABLE %s"%tmp_table)
            except cx_Oracle.DatabaseError as e:
                log.error("DB EXCEPTION: %s" %e)
                sys.exit(1)
            except Exception as e:
                log.error("UNEXPECTED ERROR IN DELETING: %s" %e)
                sys.exit(1)


        log.info( "[%s] data chunk #%d is queried.[%d rows]" %(act, i, n_row))

        # empty query chunk
        if n_row == 0:
            log.info( "[%s] data chunk #%d is EMPTY! UNI_ID FROM %d TO %d" %(act, i, chunk_i[0], chunk_i[1]))

        # file creation log
        if os.path.isfile(nm):
            log.info( "[%s] data chunk #%d is CREATED!" %(act, i))
        else:
            log.info( "[%s] data chunk #%d is NOT CREATED!" %(act, i))


    log.info("QUERY FOR [%s] FINISHED!" % act)
    cursor.close()
    connection.close()
