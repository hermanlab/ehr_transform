"""
Count regex in notes .csv. Takes in a 1-level directory

"""

import logging
import time
import os, sys
import re
import pandas as pd
from multiprocessing import Pool
import gnupg
import getpass
import sqlite3


from EHR_transform.note_fx import read_notes, notes_cleaning, add_regex_counts, collapse_note_empi_date, empi_to_pts_id, deidentify
from EHR_transform.data.regex_dict import regex_dict

log = logging.getLogger(__name__)



def build_parser(parser, config, prj, pipeline_version):
    indir = os.path.join(config['Directories']['repos_dir'],
                         config['Directories']['raw_data_temp_dir'],
                         config[prj]['prj_dir'], "RAR_"+pipeline_version,
                         "notes_queried")

    outdir = os.path.join((config['Directories']['repos_dir']),
                          config['Directories']['processed_data_temp_dir'],
                          config[prj]['prj_dir'], "RAR_"+pipeline_version,
                          "notes_regex_out")
    outdir_db = os.path.join((config['Directories']['repos_dir']),
                          config['Directories']['processed_data_temp_dir'],
                          config[prj]['prj_dir'], "RAR_"+pipeline_version)

    empi_pts_id_file = os.path.join((config['Directories']['repos_dir']),
                                   config['Directories']['raw_data_temp_dir'],
                                   config[prj]['prj_dir'], "RAR_"+pipeline_version,
                                   config[pipeline_version]['empi_pts_id_file'])

    de_mapping_file = os.path.join((config['Directories']['repos_dir']),
                          config['Directories']['processed_data_temp_dir'],
                          config[prj]['prj_dir'], "RAR_"+pipeline_version,
                          "mapping_files", 'de_date_mapping.csv')

    pts_id = config[pipeline_version]['pts_id']
    chunk_size = config[prj]['chunk_size']

    parser.add_argument('--indir',
                        type=str,
                        default=indir,
                        help='note files directory. default is ./data/raw_data/PROJECT_NAME/RAR_Version/notes_queried')
    parser.add_argument('--outdir',
                        type=str,
                        default=outdir,
                        help='output file directory. default is ./data/processed_data/PROJECT_NAME/RAR_Version/notes_regex_out')
    parser.add_argument('--outdir_db',
                        type=str,
                        default=outdir_db,
                        help='output file parent directory. default is ./data/processed_data/PROJECT_NAME/RAR_Version/')
    parser.add_argument('--empi_pts_id_file',
                        type=str,
                        default=empi_pts_id_file,
                        help='EMPI-UNIID file')
    parser.add_argument('--pts_id',
                        type=str,
                        default=pts_id,
                        help='pts_id for the project and current pipeline version')
    parser.add_argument('--chunk_size', '-chunk',
                        type=int,
                        default=chunk_size,
                        help="Number of EMPIs to process at once")
    parser.add_argument('--workers',
                        type=int,
                        default=5,
                        help='number of workers, [DEFAULT IS 5]')
    parser.add_argument('--gpg_indir',
                        action='store_true',
                        help='Specify this if indir is a gpg folder')
    parser.add_argument('--pk_enc_id_level',
                        action='store_true',
                        help='store data in PK_ENCOUNTER_ID level')
    parser.add_argument('--de_mapping_file',
                        type=str,
                        default=de_mapping_file,
                        help='Deidentification mapping file for date shifts')                    
    parser.add_argument('--prj',
                        type=str,
                        default=prj,
                        help='project name for defining/finding DB')
    parser.add_argument('--test',
                        action='store_true',
                        help='Enter test mode, reading in only 1000 rows for 3 files (if many)')


def worker(input_file, nrows, empi_pts_id, pts_id, outdir, gpg_indir, pk_enc_id_level, conn, gpg_pwd):

    try:
        if gpg_indir:
            gpg = gnupg.GPG(gnupghome = os.environ['HOME'] + "/.gnupg")
            tmp_file = os.path.splitext(input_file)[0]
            with open(input_file, 'rb') as f:
                    status = gpg.decrypt_file(
                        f, passphrase = gpg_pwd,
                        output=tmp_file,
                        always_trust=True)
            if not status.ok:
                log.error("FILE [%s] NOT DECRYPTED! SKIPPED THIS FILE." %input_file)
                sys.exit(1)
            else:
                log.info("DECRYPTED FILE [%s]" %input_file)
        else:
            tmp_file = input_file


        log.info("START ON NOTE FILE [%s]" % os.path.basename(tmp_file))

        df = read_notes(dat_file = tmp_file, log=log, nrows=nrows)

        # Clean note
        df = notes_cleaning(df, log=log)
        log.info("CLEANED NOTES")
        # Add regex and subset columns
        cols_in =  list(['CONTACT_SERIAL_NUM', 'EMPI', 'PAT_ENC_CSN_ID', 'ENCOUNTER_DATE', 'NOTE_ID', 'NOTE_STATUS', 'NOTE_TYPE', 'NOTE_TEXT'])
        df = add_regex_counts(notes_df = df, regex_dict = regex_dict,
                                cols_list=cols_in, log=log)

        log.info("REGEX ADDED")

        # collapse to EMPI_DATE. If not, just add EMPI_DATE column which is necessary for next function: empi_to_pts_id
        # NOTE_TEXT is dropped in this step: either in collapse_note_empi_date func, or explicitly droppped
        if not pk_enc_id_level:
            df = collapse_note_empi_date(df=df, regex_dict=regex_dict, log=log)
            log.info("COLLAPSED TO EMPI_DATE")
        else:
            if "NOTE_TEXT" in df.columns:
                df.drop("NOTE_TEXT", axis=1, inplace=True)

            df['EMPI_DATE'] = df['EMPI'] + " " + df.ENCOUNTER_DATE.dt.strftime(date_format = "%Y-%m-%d")


        # change EMPI_DATE into UNI_ID
        df = empi_to_pts_id(df, empi_pts_id = empi_pts_id,
                            pts_id=pts_id, log=log)
        if pk_enc_id_level:
            df = df.drop(["ID_DATE"], axis=1)


        # if not pk_enc_id_level:
        #     # Export df (out files will be named as regex_ + original name)
        #     outfile_nm = r"regex_" + os.path.basename(tmp_file)
        #     outfile = os.path.join(outdir,outfile_nm)
        #     df.to_csv(outfile, index=False)

        #     log.info("CHANGED TO ID_DATE: [%d] ID_DATE"%df.ID_DATE.nunique())
        #     log.info("ID_DATE LEVEL FILE FINISHED! [%s]"%os.path.basename(tmp_file))
        # else:
        #     df.to_sql('notes', conn, if_exists='append', index=False)
        #     log.info("NOTE COUNT INTO DB SUCCESSFUL! [%s]" %os.path.basename(tmp_file))


        if gpg_indir:
            ## remove temporary .csv file
            os.remove(tmp_file)
            log.info("GPG DECRYPTION FILE [%s] REMOVED!"%tmp_file)
    except Exception as e:
        log.error("FILE [%s] SKIPPED ON ERROR %s"%(os.path.basename(tmp_file), e))

    return df

def write_out(df, conn, pk_enc_id_level):
    #' Write dataframe <df> to database connection <conn>
    #' df
    #' conn

    if pk_enc_id_level:
        df.to_sql('notes',
                  conn, if_exists='append', index=False)
#        log.info("NOTE COUNT INTO DB SUCCESSFUL! [%s]" % os.path.basename(tmp_file))
        log.info("NOTE COUNT INTO DB SUCCESSFUL!")
    else:
        log.error("Don't know who to write to files")
        sys.exit(1)

def action(args):

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # get a list of files with name and full path, in the inputd directory
    file_ls = list()
    for root, dirs, files in os.walk(args.indir):
        for name in files:
            if args.gpg_indir:
                if os.path.splitext(name)[1] != '.gpg':
                    continue
            else:
                if os.path.splitext(name)[1] == '.gpg':
                    continue

            full_name = os.path.join(root, name)
            file_ls.append(full_name)

    log.info("%d RAW NOTE FILES FOUND" %len(file_ls))

    # In test mode, only read in 4 files(if many, otherwise, read all in), 1000 rows each, output 3 files(if many, otherwise, output all)
    if args.test:
        file_ls = file_ls[0:3]
        nrows = 1000
    else:
        nrows = None

    # read in EMPI-PTS_ID file
    empi_pts_id = pd.read_csv(args.empi_pts_id_file, dtype=object)

    if args.gpg_indir:
        pwd = getpass.getpass("GPG Passphrase:")
    else:
        pwd = None

    # read in date mapping file for deidentification
    de_date_mapping = pd.read_csv(args.de_mapping_file)
    de_date_mapping[args.pts_id] = de_date_mapping[args.pts_id].astype(str)

    p = Pool(args.workers)

    if args.pk_enc_id_level:
        dbfile = os.path.join(args.outdir_db,
                          args.prj + "_pk_enc_DEID.sqlite")

        try:
            conn = sqlite3.connect(dbfile)
            if not os.path.exists(dbfile):
                log.info("NEW DB IS CREATED!")
        except Exception as e:
            log.error("DB CREATION ERROR: %s" %e)
            sys.exit(1)
    else:
        conn = None

    results = []
    for input_file in file_ls:

        # Identifying the pt ids for each chunk
        if args.gpg_indir:
            id_chunk = int(os.path.splitext(os.path.basename(input_file))[0].split("_")[1].split(".")[0])
        else:
            id_chunk = int(os.path.splitext(os.path.basename(input_file))[0].split("_")[1])
        start_ID = (id_chunk - 1) * args.chunk_size
        end_ID = start_ID + args.chunk_size
        empi_pts_id_PART = empi_pts_id[(empi_pts_id[args.pts_id].astype(int) < end_ID) & (empi_pts_id[args.pts_id].astype(int) >= start_ID)]

        keywords = { 'nrows': nrows, 'empi_pts_id': empi_pts_id_PART, 'pts_id': args.pts_id,
            'outdir': args.outdir, 'gpg_indir':args.gpg_indir,
            'pk_enc_id_level':args.pk_enc_id_level, 'conn':conn,'gpg_pwd':pwd}
        if args.workers == 1:
            results.append(worker(input_file=input_file, **keywords))
        else:
            results.append(p.apply_async(worker,  args=(input_file, ), kwds=keywords) )
    p.close()


    for x in results:
        if args.workers == 1:
            a = x
        else:
            a = x.get()

        if (a is not None) and (a.shape[0] > 0):
            a = deidentify(a, pts_id = args.pts_id, mapping_table = de_date_mapping, dt_cols = ['ENCOUNTER_DATE'])
            write_out(df=a, conn=conn, pk_enc_id_level=args.pk_enc_id_level)
            
    
    conn.cursor().execute("CREATE UNIQUE INDEX idx_NOTE_ID on notes(NOTE_ID)")
    conn.cursor().execute("CREATE INDEX idx_UNI_ID_NOTE_DT on notes(UNI_ID, ENCOUNTER_DATE)")
    conn.close()
    log.info("NOTES INTO DB FINISHED!")
