"""
Count regex in notes .csv
"""

import logging


from EHR_transform.note_fx import read_notes, notes_cleaning, display_regex_hits
from EHR_transform.data.regex_dict import regex_dict

log = logging.getLogger(__name__)

def build_parser(parser, config, prj, pipeline_version):
    parser.add_argument('infile',
                        nargs='?',
                        help='.csv file')
    parser.add_argument('regex_key',
                        nargs='?',
                        help='Which regex to query')
    parser.add_argument('--nrows',
                        type=int, default=None,
                        help='Number of rows to read from .csv')

def action(args):
    # Read note

    df = read_notes(dat_file = args.infile,
                    nrows = args.nrows, log=log)


    # Clean note
    df = notes_cleaning(df, log=log)

    display_regex_hits(df,
                       regex_dict,
                       args.regex_key)
