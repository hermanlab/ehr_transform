"""
ENCRYPT/DECRYPT FILES
"""


import logging
import os
import re
import gnupg
import sys
import getpass


log = logging.getLogger(__name__)


def build_parser(parser, config, prj, pipeline_version):
    parser.add_argument('--indir',
                        type=str,
                        help='directory of files to be encrypted/decrypted')
    parser.add_argument('--infile',
                        type=str,
                        help='file to be encrypted/decrypted')
    parser.add_argument('--outdir',
                        type=str,
                        help='directory for encrypted files. default would be in the same folder as file')
    parser.add_argument('--recipients',
                       # type=str,
                        action='append',
                        # default='xiruo.ding@uphs.upenn.edu',
                        help='recipient(s) for gpg-encrypted files')

    parser.add_argument('--encrypt',
                        action='store_true',
                        help='encryption mode')
    parser.add_argument('--decrypt',
                        action='store_true',
                        help='decryption mode')
    parser.add_argument('--not_del_origin',
                        action="store_true",
                        help='whether to delete original file if ENCRYPTION is successful. If not specified, origins will be removed')

def action(args):


    if args.outdir:
        if not os.path.exists(args.outdir):
            os.makedirs(args.outdir)


    file_ls = list()


    if args.indir:
        for root, dirs, files in os.walk(args.indir):
            for name in files:
                full_name = os.path.join(root, name)
                file_ls.append(full_name)
    elif args.infile:
        file_ls.append(args.infile)
    else:
        log.error("NEITHER indir NOR infile IS PROVIDED!")
        sys.exit(1)

    gpg = gnupg.GPG(gnupghome = os.environ['HOME'] + "/.gnupg")


    if args.encrypt:
        for file_in in file_ls:
            if os.path.splitext(file_in)[1] == '.gpg':
                continue

            if args.outdir:
                file_out = os.path.join(args.outdir, os.path.basename(file_in) + ".gpg")
            else:
                file_out = file_in + ".gpg"

            with open(file_in, 'rb') as f:
                status = gpg.encrypt_file(
                    f, recipients=args.recipients,
                    output=file_out,
                    always_trust=True)

            if status.ok:
                if not args.not_del_origin:
                    os.remove(file_in)
                    log.info("FILE [%s] ENCRYPTION SUCCESSFUL! FILE DELETED" %(os.path.basename(file_in)))
                else:
                    log.info("FILE [%s] ENCRYPTION SUCCESSFUL!" %(os.path.basename(file_in)))

            else:
                log.info("FILE [%s] ENCRYPTION FAILED!"%os.path.basename(file_in))

    elif args.decrypt:

        pwd = getpass.getpass("GPG Passphrase:")

        for file_in in file_ls:
            if os.path.splitext(file_in)[1] != '.gpg':
                continue

            if args.outdir:
                file_out = os.path.join(args.outdir, os.path.splitext(os.path.basename(file_in)))
            else:
                file_out = os.path.splitext(file_in)[0]

            with open(file_in, 'rb') as f:
                status = gpg.decrypt_file(
                    f, passphrase = pwd,
                    output=file_out,
                    always_trust=True)

                    # if status.ok:
                    #     os.remove(file_in)

            if status.ok:
                log.info("FILE [%s] DECRYPTION SUCCESSFUL!" %(os.path.basename(file_in)))
            else:
                log.info("FILE [%s] DECRYPTION FAILED!" %(os.path.basename(file_in)))
    else:
        log.error('NO MODE PROVIDED FOR GPG')
        sys.exit(1)



